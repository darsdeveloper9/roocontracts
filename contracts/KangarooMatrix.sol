// SPDX-License-Identifier: MIT
/*
https://
*/
pragma solidity 0.7.6;

import "./lib/IBEP20.sol";
import "./lib/SafeMath.sol";
import "./lib/TransferHelper.sol";
import "./KangarooStake.sol";
import "./KangarooToken.sol";
import "./lib/IPancakeRouter02.sol";
import './lib/IPancakeFactory.sol';
import './lib/IPancakePair.sol';

pragma experimental ABIEncoderV2;

contract KangarooMatrix {


    using SafeMath for uint256;
    using TransferHelper for IBEP20;
    
    struct User {
        uint32 referrerID;
        uint32 partnersCount;
        
        uint8 activeX3Levels;
        uint8 activeX6Levels;
        uint8 maxAvailableLevel;
        address userAddress;
        uint256 lastActivity;
        bool refBlocked;
       
        mapping(uint8 => X3) x3Matrix;
        mapping(uint8 => X6) x6Matrix;
    }
    
    struct X3 {
        bool blocked;
        uint32 currentReferrerID;
        uint32[] referralsID; 
        uint32 reinvestCount;
    }
    
    struct X6 {
        bool blocked;
        uint32 currentReferrerID;
        uint32[] firstLevelReferralsID;
        uint32[] secondLevelReferralsID;
        uint32 reinvestCount;

        uint32 closedPart;
    }

    struct MaxL{
        address user;
        uint8 level;
    }
  
    
    mapping(uint32 => User) public users;
    mapping(address => uint32) public AddressToId;

    uint32 public lastUserId = 2;
    address immutable public matrixesRoot;
    address immutable public distrContrAddress;
    address public levelsAdmin;
    bool public openSalePeriod=false;
    
    KangarooStake immutable public stakePool;
    IBEP20 public matrixesToken;
    IBEP20 immutable public openSaleToken;
    IPancakePair immutable public pancakePair;
    uint8 public constant LAST_LEVEL = 21;
    uint256 public matrixesTokenRate=1e18;
    mapping(uint8 => uint256) public levelPrice;
    uint256 immutable public startTimestamp;
    uint256 immutable public periodDuration; //86400 * 30 = 2592000 seconds
    

    modifier onlyRoot() {
        require(msg.sender==matrixesRoot);
        _;
    }
    modifier onlyLevelsAdmin() {
        require(msg.sender==levelsAdmin);
        _;
    }

    event Registration(address user, address referrer, uint32 userId, uint32 referrerId,uint8 matrix);
    event Reinvest(uint32 userID, uint32 currentReferrerID, uint32 callerID, uint8 matrix, uint8 level);
    event Upgrade(uint32 userId, uint32 referrerId, uint8 matrix, uint8 level);
    event NewUserPlace(uint32 userId, uint32 referrerId, uint8 matrix, uint8 level, uint8 place);
    event MissedReceive(uint32 receiverID, uint32 fromID, uint8 matrix, uint8 level);
    event SentExtraDividends(uint32 fromID, uint32 receiverID, uint256 amount, uint8 matrix, uint8 level);
    event SentDividends(uint32 fromID, uint32 receiverID, uint256 amount, uint8 matrix, uint8 level);
    event OpenSale(address _matrixToken);
    event RefBlocked(uint32 userID,bool status);
    
    constructor(KangarooStake _stakePool,
                address _matrixesRoot,
                address _distrContrAddress,
                address _levelsAdmin,
                uint256 _periodDuration) {
        
        levelPrice[1]=15;
        levelPrice[2]=23;
        levelPrice[3]=34;
        levelPrice[4]=51;
        levelPrice[5]=76;
        levelPrice[6]=114;
        levelPrice[7]=171;
        levelPrice[8]=257;
        levelPrice[9]=385;
        levelPrice[10]=577;
        levelPrice[11]=865;
        levelPrice[12]=1300;
        levelPrice[13]=1950;
        levelPrice[14]=2920;
        levelPrice[15]=4380;
        levelPrice[16]=6570;
        levelPrice[17]=9855;
        levelPrice[18]=14780;
        levelPrice[19]=22170;
        levelPrice[20]=33255;
        levelPrice[21]=49880;

        periodDuration=_periodDuration;


        matrixesRoot = _matrixesRoot;
        openSaleToken=_stakePool.rooToken(); //IBEP20(_openSaleToken);
        matrixesToken=_stakePool.usdtToken(); //IBEP20(_matrixesToken);
        pancakePair=IPancakePair(_stakePool.PancakePairAddress());
        distrContrAddress=_distrContrAddress;
        stakePool=_stakePool;
        levelsAdmin=_levelsAdmin;
        startTimestamp=block.timestamp;

        users[1].activeX3Levels=LAST_LEVEL;
        users[1].activeX6Levels=LAST_LEVEL;
        users[1].userAddress=_matrixesRoot;
        AddressToId[_matrixesRoot]=1;

    }

    function setAdmin(address _levelsAdmin) external onlyRoot {
        levelsAdmin=_levelsAdmin;
    }

    function setRefBlocked(uint32 _userID,bool _status) external onlyLevelsAdmin {
        require(
            _userID>0 
            && isUserExists(users[_userID].userAddress), "user not exists"
        );
        users[_userID].refBlocked=_status;

        emit RefBlocked(_userID,_status);
    }

    function switchToOpenSale() external onlyRoot {   
        require(KangarooToken(address(openSaleToken)).startOpenSale(),"cant start OpenSale period");
        require(stakePool.startOpenSale(),"StakePool:cant start OpenSale period");
        matrixesToken=openSaleToken;
        openSalePeriod=true;
        emit OpenSale(address(openSaleToken));
    }


    function _updateMatrixesTokenRate() internal {
        
        if(openSalePeriod){
            (uint112 reserves0, uint112 reserves1,) = pancakePair.getReserves();
            (uint112 reserveIn, uint112 reserveOut) = pancakePair.token0() == address(openSaleToken) ? (reserves0, reserves1) : (reserves1, reserves0);
        
            if (reserveIn > 0 && reserveOut > 0 && 1e18 < reserveOut){
                uint256 numerator = uint256(1e18).mul(10000).mul(reserveIn);
                uint256 denominator = uint256(reserveOut).sub(1e18).mul(9975);
                matrixesTokenRate = numerator.div(denominator).add(1);
            }
        }

    }

    function setMaxLevel(MaxL[] calldata mlArray) external onlyLevelsAdmin {
        
        require(mlArray.length<11, "maximum mlArray size must be 10");

        for (uint256 i = 0; i < mlArray.length; i++) {
            uint8 level=mlArray[i].level;
            require(level > 6 && level <= LAST_LEVEL, "invalid level");
            uint32 userID=AddressToId[mlArray[i].user];
            require(userID>0, "user is not exists.");
            users[userID].maxAvailableLevel=level;
        }     
        
    }

    
    
    function buyNewLevel(uint8 matrix, uint8 level) external {

        uint32 userID=AddressToId[msg.sender];
        require(userID>0 && users[userID].activeX3Levels>=1 && users[userID].activeX6Levels>=1 , "user is not exists. Register first.");
        require(matrix == 1 || matrix == 2, "invalid matrix");
        require(level > 1 && level <= LAST_LEVEL, "invalid level");
        require(users[userID].maxAvailableLevel>=level,"this level not available");

        uint256 regPayAmount=getLevelPrice(level);
        require(
            matrixesToken.allowance(address(msg.sender), address(this)) >=
                regPayAmount,
            "Increase the allowance first,call the approve method"
        );

        matrixesToken.safeTransferFrom(
            address(msg.sender),
            address(this),
            regPayAmount
        );


        if (matrix == 1) {
            require(users[userID].activeX3Levels == (level-1) 
            && users[userID].activeX6Levels == (level-1), "wrong next X3 level");
            

            if (users[userID].x3Matrix[level-1].blocked) {
                users[userID].x3Matrix[level-1].blocked = false;
            }
    
            uint32 freeX3ReferrerID = findFreeX3Referrer(userID, level);
            users[userID].x3Matrix[level].currentReferrerID = freeX3ReferrerID;
            users[userID].activeX3Levels = level;
            updateX3Referrer(userID, freeX3ReferrerID, level);
            
            emit Upgrade(userID, freeX3ReferrerID, matrix, level);

        } else {
            require(users[userID].activeX3Levels == level 
            && users[userID].activeX6Levels == (level-1), "wrong next X6 level"); 

            if (users[userID].x6Matrix[level-1].blocked) {
                users[userID].x6Matrix[level-1].blocked = false;
            }

            uint32 freeX6ReferrerID = findFreeX6Referrer(userID, level);
            
            users[userID].activeX6Levels = level;
            updateX6Referrer(userID, freeX6ReferrerID, level);
            
            emit Upgrade(userID, freeX6ReferrerID, matrix, level);
        }
        _updateMatrixesTokenRate();
    } 

    function getLevelPrice(uint8 level) public view returns(uint256) {
        
        if(level<1 || level > LAST_LEVEL){
            return 0;
        }
        return levelPrice[level].mul(matrixesTokenRate);
    }  
    
    function registrationX3(uint32 referrerID) external {
        address userAddress=msg.sender;
        require(!isUserExists(userAddress), "user already exists");
        require(
            referrerID>0 
            && isUserExists(users[referrerID].userAddress), "referrer not exists"
        );
        require(users[referrerID].refBlocked==false, "this referrer is blocked");
        require(
            users[referrerID].activeX6Levels>0, "referrer without X6 registration"
        );
        
        uint32 size;
        assembly {
            size := extcodesize(userAddress)
        }
        require(size == 0 && userAddress == tx.origin, "cannot be a contract");

        uint256 regPayAmount=getLevelPrice(1);
              
        require(
            matrixesToken.allowance(address(userAddress), address(this)) >=
                regPayAmount,
            "Increase the allowance first,call the approve method"
        );

        matrixesToken.safeTransferFrom(
            userAddress,
            address(this),
            regPayAmount
        );


        uint32 userID=lastUserId;
        lastUserId++;       
        
        
        users[userID].referrerID=referrerID;
        users[userID].activeX3Levels=1;
        users[userID].userAddress=userAddress;
        users[userID].maxAvailableLevel=6;
        

        AddressToId[userAddress]=userID;
        users[userID].x3Matrix[1].currentReferrerID = referrerID;
        updateX3Referrer(userID, referrerID, 1);

        _updateMatrixesTokenRate();
       
        emit Registration(userAddress, users[referrerID].userAddress, userID, referrerID,1);
    }

    function registrationX6() external {

        uint32 userID=AddressToId[msg.sender];
        require(userID>0 && users[userID].activeX3Levels==1,"activate the X3 first");
        require(users[userID].activeX6Levels==0,"X6 already activated");
        
        uint256 regPayAmount=getLevelPrice(1);
              
        require(
            matrixesToken.allowance(address(msg.sender), address(this)) >=
                regPayAmount,
            "Increase the allowance first,call the approve method"
        );

        matrixesToken.safeTransferFrom(
            address(msg.sender),
            address(this),
            regPayAmount
        );

        uint32 referrerID=users[userID].referrerID;
        uint256 lastActivity=getCurrentPeriod();
        users[userID].lastActivity=lastActivity;
        users[userID].activeX6Levels=1;
        users[referrerID].partnersCount++;
        users[referrerID].lastActivity=lastActivity;
        
        updateX6Referrer(userID, referrerID, 1);
        

        require(stakePool.createUser(msg.sender),"cant create User");
        _updateMatrixesTokenRate();
       
        emit Registration(msg.sender, users[referrerID].userAddress, userID, referrerID,2);
    }
    
    function updateX3Referrer(uint32 userID, uint32 referrerID,  uint8 level) private {
        users[referrerID].x3Matrix[level].referralsID.push(userID);

        if (users[referrerID].x3Matrix[level].referralsID.length < 3) {
            emit NewUserPlace(userID, referrerID, 1, level, uint8(users[referrerID].x3Matrix[level].referralsID.length));
            return sendDividends(referrerID, userID, 1, level);
        }
        
        emit NewUserPlace(userID, referrerID, 1, level, 3);
        //close matrix
        users[referrerID].x3Matrix[level].referralsID = new uint32[](0);
        if (users[referrerID].activeX3Levels <= level && level != LAST_LEVEL) {
            users[referrerID].x3Matrix[level].blocked = true;
        }

        //create new one by recursion
        if (referrerID > 1) {
            //check referrer active level
            uint32 freeReferrerID = findFreeX3Referrer(referrerID, level);
            if (users[referrerID].x3Matrix[level].currentReferrerID != freeReferrerID) {
                users[referrerID].x3Matrix[level].currentReferrerID = freeReferrerID;
            }
            
            users[referrerID].x3Matrix[level].reinvestCount++;
            emit Reinvest(referrerID, freeReferrerID, userID, 1, level);
            updateX3Referrer(referrerID, freeReferrerID, level);
        } else {
            sendDividends(1, userID, 1, level);
            users[1].x3Matrix[level].reinvestCount++;
            emit Reinvest(1, 0, userID, 1, level);
        }
    }

    function updateX6Referrer(uint32 userID, uint32 referrerID, uint8 level) private {
        require(users[referrerID].activeX6Levels>=level, "500. Referrer level is inactive");
        
        if (users[referrerID].x6Matrix[level].firstLevelReferralsID.length < 2) {
            users[referrerID].x6Matrix[level].firstLevelReferralsID.push(userID);
            emit NewUserPlace(userID, referrerID, 2, level, uint8(users[referrerID].x6Matrix[level].firstLevelReferralsID.length));
            
            //set current level
            users[userID].x6Matrix[level].currentReferrerID = referrerID;

            if (referrerID > 1) {            
            
                uint32 ref = users[referrerID].x6Matrix[level].currentReferrerID;            
                users[ref].x6Matrix[level].secondLevelReferralsID.push(userID); 
            
                uint256 len = users[ref].x6Matrix[level].firstLevelReferralsID.length;
            
                if ((len == 2) && 
                    (users[ref].x6Matrix[level].firstLevelReferralsID[0] == referrerID) &&
                    (users[ref].x6Matrix[level].firstLevelReferralsID[1] == referrerID)) {
                    if (users[referrerID].x6Matrix[level].firstLevelReferralsID.length == 1) {
                        emit NewUserPlace(userID, ref, 2, level, 5);
                    } else {
                        emit NewUserPlace(userID, ref, 2, level, 6);
                    }
                }  else if ((len == 1 || len == 2) &&
                        users[ref].x6Matrix[level].firstLevelReferralsID[0] == referrerID) {
                    if (users[referrerID].x6Matrix[level].firstLevelReferralsID.length == 1) {
                        emit NewUserPlace(userID, ref, 2, level, 3);
                    } else {
                        emit NewUserPlace(userID, ref, 2, level, 4);
                    }
                } else if (len == 2 && users[ref].x6Matrix[level].firstLevelReferralsID[1] == referrerID) {
                    if (users[referrerID].x6Matrix[level].firstLevelReferralsID.length == 1) {
                        emit NewUserPlace(userID, ref, 2, level, 5);
                    } else {
                        emit NewUserPlace(userID, ref, 2, level, 6);
                    }
                }

                return updateX6ReferrerSecondLevel(userID, ref, level);
            }else{
                return sendDividends(1, userID, 2, level);
            }
        }
        
        users[referrerID].x6Matrix[level].secondLevelReferralsID.push(userID);

        if (users[referrerID].x6Matrix[level].closedPart != 0) {
            if (users[referrerID].x6Matrix[level].firstLevelReferralsID[0] == users[referrerID].x6Matrix[level].closedPart) {

                updateX6(userID, referrerID, level, true);
                return updateX6ReferrerSecondLevel(userID, referrerID, level);

            } else {
                updateX6(userID, referrerID, level, false);
                return updateX6ReferrerSecondLevel(userID, referrerID, level);
            }
        }

        if (users[referrerID].x6Matrix[level].firstLevelReferralsID[1] == userID) {
            updateX6(userID, referrerID, level, false);
            return updateX6ReferrerSecondLevel(userID, referrerID, level);
        } else if (users[referrerID].x6Matrix[level].firstLevelReferralsID[0] == userID) {
            updateX6(userID, referrerID, level, true);
            return updateX6ReferrerSecondLevel(userID, referrerID, level);
        }
        
        if (users[users[referrerID].x6Matrix[level].firstLevelReferralsID[0]].x6Matrix[level].firstLevelReferralsID.length <= 
            users[users[referrerID].x6Matrix[level].firstLevelReferralsID[1]].x6Matrix[level].firstLevelReferralsID.length) {
            updateX6(userID, referrerID, level, false);
        } else {
            updateX6(userID, referrerID, level, true);
        }
        
        updateX6ReferrerSecondLevel(userID, referrerID, level);
    }

    function updateX6(uint32 userID, uint32 referrerID, uint8 level, bool x2) private {
        if (!x2) {
            users[users[referrerID].x6Matrix[level].firstLevelReferralsID[0]].x6Matrix[level].firstLevelReferralsID.push(userID);
            emit NewUserPlace(userID, users[referrerID].x6Matrix[level].firstLevelReferralsID[0], 2, level, uint8(users[users[referrerID].x6Matrix[level].firstLevelReferralsID[0]].x6Matrix[level].firstLevelReferralsID.length));
            emit NewUserPlace(userID, referrerID, 2, level, 2 + uint8(users[users[referrerID].x6Matrix[level].firstLevelReferralsID[0]].x6Matrix[level].firstLevelReferralsID.length));
            //set current level
            users[userID].x6Matrix[level].currentReferrerID = users[referrerID].x6Matrix[level].firstLevelReferralsID[0];
        } else {
            users[users[referrerID].x6Matrix[level].firstLevelReferralsID[1]].x6Matrix[level].firstLevelReferralsID.push(userID);
            emit NewUserPlace(userID, users[referrerID].x6Matrix[level].firstLevelReferralsID[1], 2, level, uint8(users[users[referrerID].x6Matrix[level].firstLevelReferralsID[1]].x6Matrix[level].firstLevelReferralsID.length));
            emit NewUserPlace(userID, referrerID, 2, level, 4 + uint8(users[users[referrerID].x6Matrix[level].firstLevelReferralsID[1]].x6Matrix[level].firstLevelReferralsID.length));
            //set current level
            users[userID].x6Matrix[level].currentReferrerID = users[referrerID].x6Matrix[level].firstLevelReferralsID[1];
        }
    }
    
    function updateX6ReferrerSecondLevel(uint32 userID, uint32 referrerID, uint8 level) private {
        if (users[referrerID].x6Matrix[level].secondLevelReferralsID.length < 4) {
            return sendDividends(referrerID, userID, 2, level);
        }
        
        uint32[] memory x6 = users[users[referrerID].x6Matrix[level].currentReferrerID].x6Matrix[level].firstLevelReferralsID;
        
        if (x6.length == 2) {
            if (x6[0] == referrerID ||
                x6[1] == referrerID) {
                users[users[referrerID].x6Matrix[level].currentReferrerID].x6Matrix[level].closedPart = referrerID;
            } else if (x6.length == 1) {
                if (x6[0] == referrerID) {
                    users[users[referrerID].x6Matrix[level].currentReferrerID].x6Matrix[level].closedPart = referrerID;
                }
            }
        }
        
        users[referrerID].x6Matrix[level].firstLevelReferralsID = new uint32[](0);
        users[referrerID].x6Matrix[level].secondLevelReferralsID = new uint32[](0);
        users[referrerID].x6Matrix[level].closedPart = 0;

        if (users[referrerID].activeX6Levels <= level && level != LAST_LEVEL) {
            users[referrerID].x6Matrix[level].blocked = true;
        }
      
        
        if (referrerID > 1) {
            users[referrerID].x6Matrix[level].reinvestCount++;
            uint32 freeReferrerID = findFreeX6Referrer(referrerID, level);

            emit Reinvest(referrerID, freeReferrerID, userID, 2, level);
            updateX6Referrer(referrerID, freeReferrerID, level);
        } else {
            users[1].x6Matrix[level].reinvestCount++;
            emit Reinvest(1, 0, userID, 2, level);
            sendDividends(1, userID, 2, level);
        }
    }
    
    function findFreeX3Referrer(uint32 userID, uint8 level) public view returns(uint32) {
        
        if(userID<lastUserId && level<=LAST_LEVEL){
            while (true) {
                if (users[users[userID].referrerID].activeX3Levels>=level && isActive(users[userID].referrerID)) {
                    return users[userID].referrerID;
                }
            
                userID = users[userID].referrerID;
            }
        }
        return 1;
       
    }
    
    function findFreeX6Referrer(uint32 userID, uint8 level) public view returns(uint32) {
        
        if(userID<lastUserId && level<=LAST_LEVEL){
            while (true) {
                if (users[users[userID].referrerID].activeX6Levels>=level && isActive(users[userID].referrerID) ) {
                    return users[userID].referrerID;
                }
            
                userID = users[userID].referrerID;
            }
        }
        return 1;
    }
        
    function usersActiveX3Levels(uint32 userID, uint8 level) external view returns(bool) {
        if(userID<lastUserId && level<=LAST_LEVEL){
            return users[userID].activeX3Levels>=level;
        }
        return false;       
    }

    function usersActiveX6Levels(uint32 userID, uint8 level) external view returns(bool) {
        if(userID<lastUserId && level<=LAST_LEVEL){
            return users[userID].activeX6Levels>=level;
        }
        return false; 
    }

    function usersX3Matrix(uint32 userID, uint8 level) external view returns(uint32, uint32[] memory, bool, uint32) {
        require(userID<lastUserId && level<=LAST_LEVEL,"wrong arguments");
        return (users[userID].x3Matrix[level].currentReferrerID,
                users[userID].x3Matrix[level].referralsID,
                users[userID].x3Matrix[level].blocked,users[userID].x3Matrix[level].reinvestCount);
    }

    function usersX6Matrix(uint32 userID, uint8 level) external view returns(uint32, uint32[] memory, uint32[] memory, bool, uint32, uint32) {
        require(userID<lastUserId && level<=LAST_LEVEL,"wrong arguments");
        return (users[userID].x6Matrix[level].currentReferrerID,
                users[userID].x6Matrix[level].firstLevelReferralsID,
                users[userID].x6Matrix[level].secondLevelReferralsID,
                users[userID].x6Matrix[level].blocked,
                users[userID].x6Matrix[level].closedPart,
                users[userID].x6Matrix[level].reinvestCount);
    }
    
    function isUserExists(address user) public view returns (bool) {
        return (AddressToId[user] != 0);
    }

    function getCurrentPeriod() public view returns (uint256) {
        return block.timestamp.sub(startTimestamp).div(periodDuration);
    }

    function isActive(uint32 _userID) public view returns (bool) {
       uint256 period=getCurrentPeriod();
       return (period.sub(users[_userID].lastActivity)<=1 || _userID==1);
    }

    function findReceiver(uint32 userID, uint32 _fromID, uint8 matrix, uint8 level) private returns(uint32, bool) {
        uint32 receiver = userID;
        bool isExtraDividends;
        if (matrix == 1) {
            while (true) {
                if (users[receiver].x3Matrix[level].blocked || isActive(receiver)==false) {
                    emit MissedReceive(receiver, _fromID, 1, level);
                    isExtraDividends = true;
                    receiver = users[receiver].x3Matrix[level].currentReferrerID;
                } else {
                    return (receiver, isExtraDividends);
                }
            }
        } else {
            while (true) {
                if (users[receiver].x6Matrix[level].blocked || isActive(receiver)==false) {
                    emit MissedReceive(receiver, _fromID, 2, level);
                    isExtraDividends = true;
                    receiver = users[receiver].x6Matrix[level].currentReferrerID;
                } else {
                    return (receiver, isExtraDividends);
                }
            }
        }
        return (1, true);
    }

    function sendDividends(uint32 userID, uint32 _fromID, uint8 matrix, uint8 level) private {

        (uint32 receiverID, bool isExtraDividends) = findReceiver(userID, _fromID, matrix, level);
        if(receiverID<1){receiverID=1;}

        
        uint256 amount=getLevelPrice(level);
        uint256 amount20=amount.div(5);
        uint256 amount80=amount.sub(amount20); 
        matrixesToken.safeTransfer(
            users[receiverID].userAddress,
            amount80
        );
        
        matrixesToken.safeTransfer(
            distrContrAddress,
            amount20
        );
        
         
              
        if (isExtraDividends) {
            emit SentExtraDividends(_fromID, receiverID, amount80, matrix, level);
        }else{
            emit SentDividends(_fromID, receiverID, amount80, matrix, level);
        }
    } 
    
}