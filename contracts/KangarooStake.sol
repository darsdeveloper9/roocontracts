// SPDX-License-Identifier: UNLICENSED
/*
https://
*/
pragma solidity 0.7.6;

import "./lib/IBEP20.sol";
import "./lib/SafeMath.sol";
import "./lib/TransferHelper.sol";
import "./lib/Ownable.sol";
import "./lib/IPancakeRouter02.sol";
import './lib/IPancakeFactory.sol';

pragma experimental ABIEncoderV2;

contract KangarooStake is Ownable{
    using SafeMath for uint256;
    using TransferHelper for IBEP20;

    
    IBEP20 immutable public lpKangarooToken;
    IBEP20 immutable public rooToken;
    IBEP20 immutable public usdtToken;
    address immutable public poolInitiator;
    address immutable public pancakeRouter;// 0x10ED43C718714eb63d5aA57B78B54704E256024E;
    address public PancakePairAddress;
    address[] public tokenPath;
    bool public openSale=false;

    struct UserInfo {
        uint256 depositTimestamp;
        uint256 sharesAmount;
        uint256 initialDepositAmount;
    }

    struct PoolInfo {
        uint256 freezingPeriod;
        uint256 currentRewardPerShare;
        uint256 sharesTotal;
        mapping(address => UserInfo) usersInfo;
    }

    //pool[0]  20%
    //pool[1]  30%
    //pool[2]  50%
    PoolInfo[3] private pool;
    mapping(address => bool) public isUserExists;
    
    modifier notForPoolInitiator() {
        require(msg.sender!=poolInitiator,"not for pool initiator");
        _;
    }

    modifier poolExist(uint256 _id) {
        require(_id >= 0 && _id<3, "bad pool id");
        _;
    }

    event Stake(uint256 poolId, address user, uint256 amount);
    event PoolCharged(uint256 amount);
    event UnStake(uint256 poolId, address user, uint256 amount);
    event Dividends(uint256 poolId, address user, uint256 amount);

    constructor(address _pancakeRouter,
        address _rooToken,
        address _usdtToken,
        address _poolInitiator,
        uint256[] memory _freezingPeriod
    ) {
        tokenPath=[_usdtToken,_rooToken];
        rooToken=IBEP20(_rooToken);
        usdtToken=IBEP20(_usdtToken);
        poolInitiator=_poolInitiator;
        pancakeRouter=_pancakeRouter;
        PancakePairAddress=IPancakeFactory(IPancakeRouter02(_pancakeRouter).factory()).getPair(_usdtToken,_rooToken);
        require(PancakePairAddress != address(0), "create Pancake pair first");
        lpKangarooToken=IBEP20(PancakePairAddress);
         
        for(uint256 i=0;i<3;i++){
            pool[i].freezingPeriod=_freezingPeriod[i];
            pool[i].usersInfo[_poolInitiator].depositTimestamp = block.timestamp;
            pool[i].usersInfo[_poolInitiator].sharesAmount = 1e12;
            pool[i].usersInfo[_poolInitiator].initialDepositAmount = 0;
        }
        pool[2].sharesTotal = 1e12;
        pool[1].sharesTotal = 2e12;
        pool[0].sharesTotal = 3e12;

    }

    function firstStaking(address _user,uint256 _amount) external {
        require(msg.sender==poolInitiator,"can only be called by the pool initiator");
        require(
            isUserExists[_user],
            "user is not exists. Register first."
        );
        require(
            usdtToken.allowance(_user, address(this)) >=_amount,
            "Increase the allowance first,call the usdt-approve method "
        );

        usdtToken.safeTransferFrom(
            _user,
            address(this),
            _amount
        );

        uint256 token0amount=usdtToken.balanceOf(address(this)).div(2);

        usdtToken.safeIncreaseAllowance(pancakeRouter, token0amount);

        uint256[] memory amounts=IPancakeRouter02(pancakeRouter)
            .swapExactTokensForTokens(
            token0amount,
            0,
            tokenPath,
            address(this),
            block.timestamp + 60
        );

        uint256 token0Amt = usdtToken.balanceOf(address(this));
        uint256 token1Amt = amounts[amounts.length - 1];//rooToken.balanceOf(address(this));

        usdtToken.safeIncreaseAllowance(
            pancakeRouter,
            token0Amt
        );
        rooToken.safeIncreaseAllowance(
            pancakeRouter,
            token1Amt
        );


        (,, uint256 liquidity)=IPancakeRouter02(pancakeRouter).addLiquidity(
            tokenPath[0],
            tokenPath[1],
            token0Amt,
            token1Amt,
            0,
            0,
            address(this),
            block.timestamp + 60
        );

        UserInfo storage user = pool[2].usersInfo[_user];
        

        user.depositTimestamp = block.timestamp;
        user.sharesAmount = user.sharesAmount.add(liquidity);
        user.initialDepositAmount = user.sharesAmount.mul(pool[2].currentRewardPerShare).div(1e12);

        for(uint256 i=0;i<3;i++){
            pool[i].sharesTotal = pool[i].sharesTotal.add(liquidity);
        }

        emit Stake(2, _user, liquidity);

    }


    function createUser(address userAddress) external onlyOwner returns (bool){
        isUserExists[userAddress]=true;
        return(true);
    }

    function startOpenSale() external onlyOwner returns(bool) {
        openSale=true;
        return(openSale);
    }

    function chargePool(uint256 amount) external returns (bool){
        
        require(amount>100,"charged amount is too small");

        rooToken.safeTransferFrom(
            address(msg.sender),
            address(this),
            amount
        );
        
        uint256 chargedAmount50=amount.div(2);
        uint256 chargedAmount20=amount.div(5);
        uint256 chargedAmount30=amount.sub(chargedAmount50.add(chargedAmount20));
            
        pool[2].currentRewardPerShare=pool[2].currentRewardPerShare
        .add(chargedAmount20.mul(1e12).div(pool[2].sharesTotal))
        .add(chargedAmount30.mul(1e12).div(pool[1].sharesTotal))
        .add(chargedAmount50.mul(1e12).div(pool[0].sharesTotal));

        pool[1].currentRewardPerShare=pool[1].currentRewardPerShare
        .add(chargedAmount30.mul(1e12).div(pool[1].sharesTotal))
        .add(chargedAmount50.mul(1e12).div(pool[0].sharesTotal));

        pool[0].currentRewardPerShare=pool[0].currentRewardPerShare
        .add(chargedAmount50.mul(1e12).div(pool[0].sharesTotal));

        emit PoolCharged(amount);
        return(true);
    }

    function dividendsTransfer(uint256 _id, address _to, uint256 _amount) internal {
        
        require(openSale,"not available before the OpenSale started");

        uint256 max=rooToken.balanceOf(address(this));
        if (_amount > max) {
            _amount=max;
        }

        pool[_id].usersInfo[_to].initialDepositAmount = pool[_id].usersInfo[_to].sharesAmount
        .mul(pool[_id].currentRewardPerShare)
        .div(1e12);

        rooToken.safeTransfer(_to, _amount);
        emit Dividends(_id, _to, _amount);
    }

    

    function stake(uint256 _id, uint256 _amount) external notForPoolInitiator poolExist(_id){
        require(
            isUserExists[msg.sender],
            "user is not exists. Register first."
        );
        require(_amount > 0, "amount must be greater than 0");
        
        
        require(
            lpKangarooToken.allowance(address(msg.sender), address(this)) >=
                _amount,
            "Increase the allowance first,call the approve method"
        );

        UserInfo storage user = pool[_id].usersInfo[msg.sender];

        if (user.sharesAmount > 0) {
            uint256 dividends = calculateDividends(_id,msg.sender);
            if (dividends > 0) {
                dividendsTransfer(_id, msg.sender, dividends);
            }
        }
        
        lpKangarooToken.safeTransferFrom(
            address(msg.sender),
            address(this),
            _amount
        );

        user.depositTimestamp = block.timestamp;
        user.sharesAmount = user.sharesAmount.add(_amount);
        user.initialDepositAmount = user.sharesAmount.mul(pool[_id].currentRewardPerShare).div(1e12);
        for(uint256 i=0;i<=_id;i++){
            pool[i].sharesTotal = pool[i].sharesTotal.add(_amount);
        }
        emit Stake(_id, msg.sender, _amount);
      
    }

    // Withdraw without caring about rewards. EMERGENCY ONLY.
    function emergencyWithdraw(uint256 _id) external notForPoolInitiator poolExist(_id){
        
        UserInfo storage user = pool[_id].usersInfo[msg.sender];
        uint256 unstaked_shares = user.sharesAmount;
        require(
            unstaked_shares > 0,
            "you do not have staked tokens, stake first"
        );
        require(isTokensFrozen(_id, msg.sender) == false, "tokens are frozen");
        user.sharesAmount = 0;
        user.initialDepositAmount = 0;

        for(uint256 i=0;i<=_id;i++){
            pool[i].sharesTotal = pool[i].sharesTotal.sub(unstaked_shares);
        }
        lpKangarooToken.safeTransfer(msg.sender, unstaked_shares);
        emit UnStake(_id, msg.sender, unstaked_shares);
    }

    function unstake(uint256 _id, uint256 _amount) external notForPoolInitiator poolExist(_id){
        
        UserInfo storage user = pool[_id].usersInfo[msg.sender];

        require(
            _amount > 0 && _amount<=user.sharesAmount,"bad _amount"
        );
        require(isTokensFrozen(_id, msg.sender) == false, "tokens are frozen");

        uint256 dividends = calculateDividends(_id, msg.sender);
        if (dividends > 0) {
            dividendsTransfer(_id, msg.sender, dividends);
        }
        user.sharesAmount=user.sharesAmount.sub(_amount);
        user.initialDepositAmount = user.sharesAmount.mul(pool[_id].currentRewardPerShare).div(1e12);
        for(uint256 i=0;i<=_id;i++){
            pool[i].sharesTotal = pool[i].sharesTotal.sub(_amount);
        }
        
        lpKangarooToken.safeTransfer(msg.sender, _amount);

        emit UnStake(_id, msg.sender, _amount);
    }

    function getDividends(uint256 _id) external poolExist(_id){
        require(
            pool[_id].usersInfo[msg.sender].sharesAmount > 0,
            "you do not have staked tokens, stake first"
        );
        uint256 dividends = calculateDividends(_id, msg.sender);
        if (dividends > 0) {
            dividendsTransfer(_id, msg.sender, dividends);
        }
    }

    function calculateDividends(uint256 _id, address userAddress)
        public
        view
        returns (uint256)
    {
        return pool[_id].usersInfo[userAddress].sharesAmount
        .mul(pool[_id].currentRewardPerShare)
        .div(1e12)
        .sub(pool[_id].usersInfo[userAddress].initialDepositAmount);
    }

    function isTokensFrozen(uint256 _id, address userAddress) public view returns (bool) {
        return (pool[_id].freezingPeriod >(block.timestamp.sub(pool[_id].usersInfo[userAddress].depositTimestamp)));
    }

    function getPoolSharesTotal(uint256 _id)
        external
        view
        returns (uint256)
    {
        return pool[_id].sharesTotal;
    }

    function getUser(uint256 _id,address userAddress)
        external
        view
        returns (UserInfo memory)
    {
        return pool[_id].usersInfo[userAddress];
    }

}
