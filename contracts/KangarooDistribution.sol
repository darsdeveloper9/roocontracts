// SPDX-License-Identifier: MIT
/*
https://
*/
pragma solidity 0.7.6;
import "./lib/IBEP20.sol";
import "./lib/SafeMath.sol";
import "./lib/Ownable.sol";
import "./lib/IPancakeRouter02.sol";
import "./lib/TransferHelper.sol";
import "./KangarooStake.sol";

contract KangarooDistribution is Ownable {
    
    using SafeMath for uint256;
    using TransferHelper for IBEP20; 

    address immutable public pancakeRouter;
    IBEP20 immutable public usdtToken;//0x55d398326f99059fF775485246999027B3197955  usdt
    IBEP20 immutable public kangarooToken;
    KangarooStake immutable public stakePool;
    address immutable public philanthropyAddress;
    uint256 immutable public minDistrBalance;
    bool public chargingPool=false;

    uint256 public philanthropyPercent;
    uint256 public burnPercent;
    uint256 public poolPercent;

    constructor (KangarooStake _stakePool,
                address _philanthropyAddress) {
        
        pancakeRouter=_stakePool.pancakeRouter();
        kangarooToken=_stakePool.rooToken(); 
        usdtToken=_stakePool.usdtToken(); 
        stakePool = _stakePool;
        philanthropyAddress=_philanthropyAddress;
        philanthropyPercent=10;
        burnPercent=40;
        poolPercent=50;
        minDistrBalance=10**18;
    }

    function setDistributionPercents(uint256 _philanthropyPercent,uint256 _burnPercent,uint256 _poolPercent) external onlyOwner {
        require(_philanthropyPercent.add(_burnPercent.add(_poolPercent))==100,"bad percent");
        philanthropyPercent =_philanthropyPercent;
        burnPercent =_burnPercent;
        poolPercent =_poolPercent;
    }

    function startChargingPool() external onlyOwner{
        chargingPool=true;
    }

    function distribute() external{
        
        uint256 usdtBalance=usdtToken.balanceOf(address(this));
        
        if(usdtBalance>=minDistrBalance){
            usdtToken.safeIncreaseAllowance(pancakeRouter, usdtBalance);
            address[] memory tokenPath = new address[](2);
            tokenPath[0] = address(usdtToken);
            tokenPath[1] = address(kangarooToken);

            IPancakeRouter02(pancakeRouter)
                .swapExactTokensForTokens(
                usdtBalance,
                0,
                tokenPath,
                address(this),
                block.timestamp + 60
            );
        }

        uint256 kangarooBalance = kangarooToken.balanceOf(address(this));

        if(kangarooBalance>minDistrBalance){
            
            if(chargingPool){
                
                uint256 toPoolAmount=kangarooBalance.mul(poolPercent).div(100);
                uint256 toBurnAmount=kangarooBalance.mul(burnPercent).div(100);
                uint256 toPhilanthropyAmount=kangarooBalance.mul(philanthropyPercent).div(100);

                if(toPoolAmount>0){
                    kangarooToken.safeIncreaseAllowance(address(stakePool), toPoolAmount);
                    require(stakePool.chargePool(toPoolAmount),"cant charge Pool");
                }

                if(toBurnAmount>0){
                    (bool success,) = address(kangarooToken).call(abi.encodeWithSignature("burn(uint256)",toBurnAmount));
                    require(success,"burn FAIL");
                }

                if(toPhilanthropyAmount>0){
                    kangarooToken.safeTransfer(philanthropyAddress, toPhilanthropyAmount);
                }
            }else{
                if(kangarooBalance>0){
                    (bool success,) = address(kangarooToken).call(abi.encodeWithSignature("burn(uint256)",kangarooBalance));
                    require(success,"burn FAIL");
                }
            }

        }

    }

}