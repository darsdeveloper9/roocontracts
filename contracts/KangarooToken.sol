// SPDX-License-Identifier: MIT
/*
https://
*/
pragma solidity 0.7.6;

import "./lib/BEP20.sol";
import "./lib/SafeMath.sol";

contract KangarooToken is BEP20 {
    using SafeMath for uint256;
    uint256 constant public MaxSupply = 21000000*1e18;
    address immutable public matrixesRoot;
    bool public openSale=false;
    mapping(address => bool) private whitelistBeforeOpenSale;


    event Burned(uint256 burnAmount);

    constructor(address _matrixesRoot) BEP20("KangarooToken", "ROO", 18) {
        matrixesRoot=_matrixesRoot;
        whitelistBeforeOpenSale[_matrixesRoot]=true;
        _mint(matrixesRoot, MaxSupply);
    }

    function transfer(address recipient, uint256 amount) public override(BEP20) returns (bool){
        require(openSale==true || whitelistBeforeOpenSale[recipient],
        "token purchase is not available during the liquidity creation period");
        return BEP20.transfer(recipient, amount);
    }

    function setWhitelistAddress(address _whitelistAddress) external onlyOwner {
        whitelistBeforeOpenSale[_whitelistAddress]=true;
    }

    function startOpenSale() external onlyOwner returns(bool) {
        openSale=true;
        return(openSale);
    }
    
    function burn(uint256 amount) external {
        _burn(msg.sender, amount);
        emit Burned(amount);
    }

}
